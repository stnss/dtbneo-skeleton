BINARY_NAME=skeleton
build:
	@go build -o bin/${BINARY_NAME} cmd/api/main.go

run: build
	@./bin/${BINARY_NAME}