package logger

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/stnss/dtbneo_skeleton/pkg/config"
	"os"
)

type Logger struct {
	*logrus.Logger
}

var logger *Logger

func SetUpLogger() {
	logger = &Logger{logrus.New()}
	logger.Formatter = &logrus.JSONFormatter{}
	logger.SetOutput(os.Stdout)

	if config.GetConfig().AppDebug {
		logger.SetLevel(logrus.DebugLevel)
	}
}

func GetLogger() *Logger {
	return logger
}
