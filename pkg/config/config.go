package config

import (
	"github.com/fsnotify/fsnotify"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"io/ioutil"
	"strings"
)

type Config struct {
	AppConfig      `mapstructure:",squash"`
	DatabaseConfig `mapstructure:",squash"`
}

var cnf = &Config{}

func LoadAllConfigs() error {
	err := loadConfig()
	if err != nil {
		return err
	}

	err = viper.Unmarshal(&cnf)
	if err != nil {
		return err
	}

	return nil
}

// FiberConfig func for configuration Fiber app.
func FiberConfig() fiber.Config {
	// Return Fiber configuration.
	return fiber.Config{
		AppName:       cnf.AppName,
		StrictRouting: false,
		CaseSensitive: false,
	}
}

func GetConfig() *Config {
	return cnf
}

func loadConfig() error {
	files, err := ioutil.ReadDir("./config") //read the files from the directory
	if err != nil {
		return err
	}

	viper.AddConfigPath("./config")
	for _, file := range files {
		viper.SetConfigType("json")
		viper.SetConfigName(getConfigNameWithoutExt(file.Name()))
		err = viper.MergeInConfig()
		if err != nil {
			return err
		}
	}

	viper.AutomaticEnv()

	viper.AddConfigPath(".")
	viper.SetConfigName(".env")
	viper.SetConfigType("env")
	_ = viper.MergeInConfig()

	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		logrus.Infof("Config %v was change", e.Name)
	})

	return nil
}

func getConfigNameWithoutExt(fileName string) string {
	after, _ := strings.CutPrefix(fileName, "config/")
	after, _ = strings.CutSuffix(after, ".json")
	return after
}
