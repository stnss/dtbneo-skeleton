package config

// AppConfig holds the AppConfig configuration
type AppConfig struct {
	AppName  string `mapstructure:"app_name"`
	AppHost  string `mapstructure:"app_host"`
	AppPort  int    `mapstructure:"app_port"`
	AppDebug bool   `mapstructure:"app_debug"`
	AppEnv   string `mapstructure:"app_env"`
}
