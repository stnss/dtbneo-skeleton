package config

// DatabaseConfig holds the DatabaseConfig configuration
type DatabaseConfig struct {
	DBHost          string `mapstructure:"db_host"`
	DBPort          int    `mapstructure:"db_port"`
	DBName          string `mapstructure:"db_name"`
	DBUser          string `mapstructure:"db_user"`
	DBPassword      string `mapstructure:"db_password"`
	MaxOpenConn     int    `mapstructure:"db_max_open_conn"`
	MaxIdleConn     int    `mapstructure:"db_max_idle_conn"`
	MaxConnLifetime int    `mapstructure:"db_conn_lifetime"`
	MaxIdleTime     int    `mapstructure:"db_idle_time"`
}
