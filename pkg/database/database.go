package database

import (
	"fmt"
	"github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"gitlab.com/stnss/dtbneo_skeleton/pkg/config"
	"time"
)

type Database struct {
	*sqlx.DB
}

var db = &Database{}

func (*Database) connect(dbConfig config.DatabaseConfig) (err error) {
	conf := mysql.NewConfig()
	conf.Net = "tcp"
	conf.Addr = fmt.Sprintf("%v:%v", dbConfig.DBHost, dbConfig.DBPort)
	conf.User = dbConfig.DBUser
	conf.Passwd = dbConfig.DBPassword
	conf.DBName = dbConfig.DBName

	dsn := conf.FormatDSN()
	db.DB, err = sqlx.Connect("mysql", dsn)
	if err != nil {
		return err
	}
	defer db.Close()

	if err = db.Ping(); err != nil {
		return err
	}

	db.SetMaxIdleConns(dbConfig.MaxIdleConn)
	db.SetMaxOpenConns(dbConfig.MaxOpenConn)
	db.SetConnMaxLifetime(time.Duration(dbConfig.MaxConnLifetime) * time.Hour)
	db.SetConnMaxIdleTime(time.Duration(dbConfig.MaxIdleTime) * time.Hour)

	return nil
}

func ConnectDatabase() error {
	return db.connect(config.GetConfig().DatabaseConfig)
}

func GetDatabase() *Database {
	return db
}
