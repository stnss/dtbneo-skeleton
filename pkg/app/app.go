package app

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/requestid"
	"github.com/google/uuid"
	"gitlab.com/stnss/dtbneo_skeleton/pkg/config"
	"strings"
)

type App struct {
	*fiber.App
}

var appServer *App

func InitializeApp() {
	f := fiber.New(config.FiberConfig())
	f.Use(
		cors.New(cors.Config{
			MaxAge: 300,
			AllowOrigins: strings.Join([]string{
				"http://*",
				"https://*",
			}, ","),
			AllowHeaders: strings.Join([]string{
				"Origin",
				"Content-Type",
				"Accept",
			}, ","),
			AllowMethods: strings.Join([]string{
				fiber.MethodGet,
				fiber.MethodPost,
				fiber.MethodPut,
				fiber.MethodDelete,
				fiber.MethodHead,
			}, ","),
		}),
		requestid.New(requestid.Config{
			ContextKey: "refid",
			Header:     "X-Reference-Id",
			Generator: func() string {
				return uuid.New().String()
			},
		}),
	)

	f.Get("/piye", func(c *fiber.Ctx) error {
		res := struct {
			Message string `json:"message"`
		}{
			Message: "Waras!",
		}
		return c.JSON(res)
	})

	appServer = &App{
		App: f,
	}
}

func (app *App) StartServer() (err error) {
	cnf := config.GetConfig().AppConfig
	return app.Listen(fmt.Sprintf("%v:%v", cnf.AppHost, cnf.AppPort))
}

func GetServer() *App {
	return appServer
}
