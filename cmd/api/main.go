package main

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/stnss/dtbneo_skeleton/pkg/app"
	"gitlab.com/stnss/dtbneo_skeleton/pkg/config"
	"gitlab.com/stnss/dtbneo_skeleton/pkg/database"
	"gitlab.com/stnss/dtbneo_skeleton/pkg/logger"
)

func main() {
	if err := config.LoadAllConfigs(); err != nil {
		logrus.WithError(err).
			Fatal("Failed to load configuration file")
	}

	logger.SetUpLogger()
	logr := logger.GetLogger()

	// Remove this code below if no need database
	if err := database.ConnectDatabase(); err != nil {
		logr.WithError(err).
			Fatal("Failed to connect to database")
	}

	app.InitializeApp()
	application := app.GetServer()

	// Put route here

	if err := application.StartServer(); err != nil {
		logr.WithError(err).
			Fatal("Failed to start server")
	}
}
